const chai = require('chai');
const expect = chai.expect;
const math = require('./math');

describe('Test chai',()=>{
    it('should compare by expect',()=>{
        expect(1).to.equal(1);
    });
    it('should compare another things by expect',()=>{
        expect(5>8).to.be.false;
        expect({name:'oh'}).to.deep.equal({name:'oh'});
        expect({name:'oh'}).to.have.property('name').to.equal('oh');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('oh').to.be.a('string');
        expect('oh'.length).to.equal(2);
        expect('oh').lengthOf(2);
        expect([1,2,3,4,5]).to.lengthOf(5);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('Math module',()=>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('add(1,1) ควรส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equal(2);
        });
    });
});